<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name'      => 'satrio',
                'email'     => 'm.s.n.thread@gmail.com',
                'remember_token' => Str::random(10),
                'role_id'   => 1
            ],
            [
                'name'      => 'test student',
                'email'     => 'students@gmail.com',
                'remember_token' => Str::random(10),
                'role_id'   => 2
            ]
        ];

        foreach ($user as $users) {
            User::create(array(
                'name'  => $users['name'],
                'email' => $users['email'],
                'password' => bcrypt('password'),
                'remember_token' => $users['remember_token'],
                'role_id'   => $users['role_id']
            ));
        }
    }
}
