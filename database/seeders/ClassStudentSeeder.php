<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ClassStudent;

class ClassStudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $className = [
            [
                'class_name'    => 12,
                'major'         => 'IPA',
                'teacher_room_name' => 'Admin'
            ],
            [
                'class_name'    => 12,
                'major'         => 'IPS',
                'teacher_room_name' => 'User Admin'
            ],
            [
                'class_name'    => 12,
                'major'         => 'TKJ',
                'teacher_room_name' => 'Guru'
            ]

        ];

        foreach ($className as $class) {
            ClassStudent::create(array(
                'class_name'    => $class['class_name'],
                'major'         => $class['major'],
                'teacher_room_name' => $class['teacher_room_name'],
            ));
        }
    }
}
