<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role = [
            [
                'name' => 'Admin',
            ],
            [
                'name' => 'Student'
            ]
        ];

        foreach ($role as $roles) {
            Role::create(array(
                'name' => $roles['name'],
            ));
        }
    }
}
