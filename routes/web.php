<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/students', [App\Http\Controllers\StudentController::class, 'index'])->name('student');
    Route::get('/class', [App\Http\Controllers\ClassController::class, 'index'])->name('class');
});


// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
