<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassStudent extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $fillable = [
        'class_name',
        'major',
        'teacher_room_name',
    ];
}
